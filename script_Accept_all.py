# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#####Libraries####


import argparse
import lib.gstreamer as gstreamer
import lib.utils as utils
from edgetpu.detection.engine import DetectionEngine
from edgetpu.classification.engine import ClassificationEngine
import serial
import threading
import os
import datetime
import random


# now = datetime.datetime.now()
# print("Current date and time: ")
# print(str(now))


ser = serial.Serial("/dev/ttyACM0", 115200,timeout=5) #config serial
list_commands = ["f","b","l","r","s","s","s","s"] #list random command
lap=0

####Functions graphics####
#to draw rectangle around detected objects 

def draw_rectangle(draw, coordinates, color, width=1): 
    for i in range(width):
        rect_start = (coordinates[0] - i, coordinates[1] - i)
        rect_end = (coordinates[2] + i, coordinates[3] + i)
        draw.rectangle((rect_start, rect_end), outline = color)




#####Functions detect####


def init_engine(model):
    """Returns an Edge TPU classifier for the model"""
    # TODO: Instantiate a ClassificationEngine for the given model
    return DetectionEngine(model)

def input_size(engine):
    """Returns the required input size for the model"""
    # TODO: Return the proper input size for the given model
    _, h, w, _ = engine.get_input_tensor_shape()
    return w, h

def inference_time(engine):
    """Returns the time taken to run inference"""
    # TODO: Return the inference time from the ClassificationEngine
    return engine.get_inference_time()

def detect_image(tensor, engine, labels):
    """Runs inference on the provided input tensor and
    returns an overlay to display the inference results
    """
    # TODO: Run inference on the provided input tensor
    results = engine.DetectWithInputTensor(
        tensor, threshold=0.7, top_k=2)

    return [(i.bounding_box.flatten().tolist(),labels[i.label_id]) for i in results]

#############   SOUNDS TO PLAY #################

f_adressed = "/home/mendel/wav_files" # sound files directory
n= 0
list_file = [] #creating a list for the paths of the different sounds

for root, dirs, files in os.walk(f_adressed): #going through folder to gather all of the sounds paths
    for file in files:
        if file.endswith('.wav'):
            list_file.append(file)

#print(list_file)

def speaker(list_to_play): # playing one sound after the other and going back to the beggining of the list when done
    sound_to_play=f_adressed+"/"+list_to_play[n]
  #  print(text_to_play)
    os.system('aplay {}'.format(sound_to_play))
    global n
    n+=1
    if n==len(list_to_play):
        n=0
 #       print(n)
        # do stuff
global speaker_thread
speaker_thread = threading.Thread(target=speaker, args=[list_file])



#####Main####
#####Checking if everything ok###############


def main(args):
    middle= 0.5
    input_source = "{0}:YUY2:{1}:{2}/1".format(args.source, args.resolution, args.frames)
    #print("input ok")
    labels = utils.load_labels(args.labels)
    #print("label ok")
    engine = init_engine(args.model)
    #print("engine ok")
    inference_size = input_size(engine)
    #print("inference ok")
    def frame_callback(tensor, layout, command):
        results = detect_image(tensor, engine, labels)
        # print("results ok")
        time = inference_time(engine)
        # ser = serial.Serial("/dev/ttyACM0", 115200)
        serial_buffer=ser.in_waiting
        if (serial_buffer>=1024):
            ser.reset_input_buffer()

        if results and time:
            

    #####################                                      #####################
    #####################   Code for detecting person and      #####################
    #####################               defining intervals     #####################
    #####################                                       #####################

            # define interval for center, left and right in the display
            
            if (serial_buffer<1024):
                if (results[0][1]== "person"): # condition if the object detected is a person

                    box_left_corner = results[0][0][0]
                    box_right_corner = results[0][0][2]


        # give command if it is on the left, on the right or in the middle.
        # send command via serial to arduino


        ##################################                                       ##################################
        ##################################    Code for moving motors according   ##################################
        ##################################               to object               ##################################
        ##################################                                       ##################################
        			if box_right_corner - box_left_corner <0.20 and box_right_corner - box_left_corner >0.05:  #person too small so move forward
                        ser.write(str.encode('f')) 
                        #print("move forward") 


                    elif box_right_corner < middle : #person on the left
                        ser.write(str.encode('l')) 
                            
                        #print("move left") 

                    elif box_left_corner > middle : #person on the right
                        ser.write(str.encode('r')) 
                        
                        #print("move right")

                    elif  box_right_corner - box_left_corner <0.05:  #person too small so move forward
                            # if not speaker_thread.is_alive():
                            #     speaker_thread = threading.Thread(target=speaker, args= [f_])
                            #     speaker_thread.start()
                        ser.write(str.encode('f'))
                        #print("f_general")
                        
                            

                    elif box_right_corner - box_left_corner  >0.70: #person too big so move backward
                        ser.write(str.encode('b')) 
                        #print("move backward") 

                    else:
                        
                        ser.write(str.encode('s'))
                             #stops if nothing happens
                        if not speaker_thread.is_alive():
                            global speaker_thread
                            speaker_thread = threading.Thread(target=speaker, args= [list_file])
                            speaker_thread.start()
                        #print("f_adressed")
                
                ###Try out to make stop sign"
                for i in 2:
                    if (results[0][i]== "stop sign"): # condition if the object detected is a stop sign
                        ser.write(str.encode('s'))
                        ser.write(str.encode('r'))    # move to the right              


                else:
                    #print("detection non humaine")

        else:
            #print("pas de detection")
            if lap%2500==0:
                if (serial_buffer<1024):
                    command=random.choice(list_commands)
                    ser.write(str.encode(command))
                    # ser.write(str.encode(s))
            else
                if (serial_buffer<1024):
                    ser.write(str.encode("s"))
            lap+=1


        print(time)
        print (serial_buffer)
   

    gstreamer.run(inference_size, frame_callback,
        source=input_source,
        loop=False,
        display=gstreamer.Display.NONE
        )


if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument('--source',
                        help='camera device (e.g. /dev/video0)',
                        default='/dev/video0')
        parser.add_argument('--resolution',
                        help='camera capture resolution',
                        default='1280x720')
        parser.add_argument('--frames',
                        help='camera capture frame rate',
                        default='30')
        parser.add_argument('--model', required=True,
                        help='.tflite model path')
        parser.add_argument('--labels', required=True,
                        help='label file path')
        args = parser.parse_args()

        main(args)
    except KeyboardInterrupt:
        pass 
