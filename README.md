# Accept-All

This is the code repository for Accept-All
---
What would happen if all the cookies and algorithms that follow us on the internet were also present in physical space?

## Name
Accept_All documentation - how to build your own autonomous robot

## Description
This gitlab is the documentation for accept-all, a robotic performance created by Guillaume Slizewicz. You can read more about Accept-All [here](https://studio.guillaumeslizewicz.com/post/658424597180563456/acceptall) 

## Visuals
![a child is playing with a robot](Gif/Accept-All.gif)

![An adult is playing with a robot during the performance accept-all, with feet in the foreground and a painting in the background](photos/Accept_All_Simon_Fusillier2.jpg)

Photo by Simon Fusillier

![A technical drawing of one of the robots of Accept-All](Technical_drawing/Accept_All_Technical_drawing_right@4x.png)

Robot by Clément Chaubet and Côme Rouanet


<iframe src="https://player.vimeo.com/video/595142856?h=9cc9d915c2" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/595142856">Accept All - Le Pavillon Namur</a> from <a href="https://vimeo.com/user94241345">Guillaume Slizewicz</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

## Installation

This project is using a modified smart car similar to this [one](https://www.elegoo.com/products/elegoo-smart-robot-car-kit-v-3-0-plus)

For this robot, you will need to install a [coral board](https://coral.ai/docs/dev-board/get-started#requirements) with its own os, mendel.

In our experience (connecting to the coral board from a Mac via ssh), it has been useful to install SAMBA (as explained on this [website](http://curlybraces.be/wiki/Ressources::RaspberryPi#Installer_et_configurer_Samba_.5Bfacultatif.5D))

It is also useful to install those [examples](https://github.com/google-coral/examples-camera) (especially the one using [OpenCV](https://github.com/google-coral/examples-camera/tree/master/opencv))

We will use its file structure and put the script /script_Accept_all.py in the folder /home/mendel/google-coral/examples-camera/opencv/

The boot-up script /boot_accept_all.rtf will have to be placed in the home folder and made into a boot-up service script via this [method](https://www.linode.com/docs/guides/start-service-at-boot/).

We will also need to upload the arduino script onto your arduino

## Usage
To check if the script works, you can run the examples provided by Google Coral, if they do, you can proceed and run :
`python3 /home/mendel/google-coral/examples-camera/opencv/script_Accept_all.py --model /home/mendel/google-coral/examples-camera/all_models/mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite  --labels /home/mendel/google-coral/examples-camera/all_models/coco_labels.txt`

## License

This Project is released under a Creative Commons License :
Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)

## Project status
- This project is still in development
